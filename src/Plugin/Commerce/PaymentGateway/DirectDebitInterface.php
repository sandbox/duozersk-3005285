<?php

namespace Drupal\commerce_directdebit\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsUpdatingStoredPaymentMethodsInterface;

/**
 * Provides the interface for the direct debit payment gateways.
 */
interface DirectDebitInterface extends OnsitePaymentGatewayInterface, SupportsUpdatingStoredPaymentMethodsInterface {

}
